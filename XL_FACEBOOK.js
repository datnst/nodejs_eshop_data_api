var request = require('request-promise');
var page_id = '102379024522561';
var page_access_token = 'EAAQSYGjZA6CsBAKySwHpzGZBJBYhN5t1S5Lm1S29ErIYkLNKydpMkbIisEGVJ8aNS3nkxcdbXJoQSEOVh9uKhn3B4velCU3ivZCz37cqLcrkmFcHZBImudKjNLWJLHHFnz3fFMCYq8ySnYDOAe8JZCsTIuytwrXq3Sstw5I2BUzG4rbS3l6BO';
exports.post_new_feed = async (content) => {
    var config = {
        method: 'POST',
        uri: `https://graph.facebook.com/v2.8/${page_id}/feed`,
        qs: {
            access_token: page_access_token,
            message: content
        }
    }
}